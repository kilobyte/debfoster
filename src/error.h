#ifndef _ERROR_H
#define _ERROR_H

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>

typedef enum {
	ERROR_NONE,
	ERROR_USER,
	ERROR_CONFIG,
	ERROR_SYSTEM,
	ERROR_INTERNAL
} error_t;

extern void perror_exit(error_t, const char *msg);
extern void error_exit(error_t, const char *msg, ...);

extern void *xmalloc(size_t);
extern char *xstrdup(const char *);
extern char *xstrndup(const char *, int);
extern char *xstrcat(const char *, const char *);

#endif
