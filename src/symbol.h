#ifndef _SYMBOL_H
#define _SYMBOL_H

typedef char *symbol_t;

extern symbol_t symbol(const char *);
extern symbol_t nsymbol(const char *, unsigned length);
extern int symcmp(symbol_t, symbol_t);
extern void symbol_dump(void);

#endif
