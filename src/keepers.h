#ifndef _KEEPERS_H
#define _KEEPERS_H

#include "AVLTree.h"

extern AVLTree *keepers, *nokeepers;

extern void openkeepers(int);
extern void closekeepers(void);
extern void writekeepers(void);
extern void readkeepers(void);

#endif
