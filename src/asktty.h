#ifndef _ASKTTY_H
#define _ASKTTY_H

extern int tty_ask(char *,char *, ...);
extern void tty_init(void);
extern void tty_restore(void);

#endif
