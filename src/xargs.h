#ifndef _XARGS_H
#define _XARGS_H

#include "AVLTree.h"

extern int runv(char *, ...);
extern int xargs(AVLTree *, const char *);

#endif
