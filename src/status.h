#ifndef _STATUS_H
#define _STATUS_H

#include "AVLTree.h"
#include "symbol.h"

struct package {
	symbol_t name;
	AVLTree depends;		/* list of packages this one depends on directly */
	AVLTree dependents;		/* list of packages depending on this one */
	AVLTree provides;		 /* list of dependencies met by this package */
        AVLTree providers;		/* list of packaged providing this dependency */
	int provider_count;		/* number of alternative packages providing this dependency */
	int orphan_depends;		/* number of orphan packages this one depends on (used for sorting) */
        unsigned short installed:1;	/* Is this an installed package? */
	unsigned short task:1;		/* This is a virtual task package */
	unsigned short keep:1;		/* Keep this package in any case */
	unsigned short nokeep:1;	/* Don't keep this package unless there is a dependency */
};

extern int pkgcmp(struct package *a, struct package *b);
extern struct package *pkg_find(symbol_t s);

extern AVLTree *packages;

extern void readstatus(void);
extern symbol_t guessbase(symbol_t);

extern char *chop(char *);
extern int chomp(char *);

#endif
