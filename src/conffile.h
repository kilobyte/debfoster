#ifndef _CONFIG_H
#define _CONFIG_H

typedef struct option_t {
	const char *key;
	void (*set)(const char *s);
} *option_t;

extern void readconfig(const char *);
extern option_t find_option(const char *);

extern const char *KeeperFile;
extern const char *DpkgStatus;
extern const char *DpkgAvailable;
extern const char *InstallCmd;
extern const char *RemoveCmd;
extern const char *InfoCmd;
extern const char *MaxPriority;
extern const char *KeepSections;
extern const char *NokeepSections;
extern const char *GuessDepends;

extern int Quiet;
extern int Force;
extern int Verbose;

extern int UseHold;
extern int UseEssential;
extern int UsePreDepends;
extern int UseRecommends;
extern int UseSuggests;
extern int UseTasks;

extern int NegativeKeepers;

#endif
