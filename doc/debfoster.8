.Dd 2001-06-10
.Dt DEBFOSTER 8
.Os "Debian GNU/Linux"
.Sh NAME
.Nm debfoster
.Nd weed unnecessary Debian packages
.Sh SYNOPSIS
.Nm
.Op Fl acdefhiknopqrstvV
.Op Fl -verbose
.Op Fl -version
.Op Fl -help
.Op Fl -quiet
.Op Fl -force
.Op Fl -mark-only
.Op Fl -upgrade
.Op Fl -config Ar file
.Op Fl -keeperfile Ar file
.Op Fl -no-keeperfile
.Op Fl -ignore-default-rules
.Op Fl -show-keepers
.Op Fl -show-orphans
.Op Fl -show-depends Ar package
.Op Fl -show-dependents Ar package
.Op Fl -show-providers Ar package
.Op Fl -show-related Ar package
.Op Fl -use-tasks
.Op Fl -option Ar opt=val
.Op Ar package1 No ...
.Op Ar package2- No ...
.Sh DESCRIPTION
.Nm
maintains a list of installed packages that were explicitly requested
rather than installed as a dependency.
Arguments are entirely optional,
.Nm
can be invoked per se after each run of dpkg and/or apt-get.
.Pp
Alternatively you can use
.Nm
to install and remove packages by specifying the packages on the command line.
Packages suffixed with a
.Ar -
are removed while packages without a suffix are installed.
.Pp
If a new package is encountered or if
.Nm
notices that a package that used to be a dependency is now an orphan,
it will ask you what to do with it.
If you decide to keep it,
.Nm
will just take note and continue.
If you decide that this package is not interesting enough it will be
removed as soon as
.Nm
is done asking questions.
If your choices cause other packages to become orphaned more
questions will ensue.
.Pp
Whenever
.Nm
asks you about a package, any of the following responses can be given:
.Pp
.Bl -tag -offset "12" -width "3456123456" -compact
.It Ql y
Yes, keep the package. This is the default response.
.It Ql n
No, delete the package.
.It Ql p
Prune the package. This tells
.Nm
to also delete all packages that are
only installed because this package depends on them.
A list of such packages, if any, is shown above the prompt.
.It Ql s
Skip this question. The next time you run
.Nm
it will ask you again about this package.
.It Ql h
Print a help message.
.It So i Sc or So \&? Sc
Show information about the package.
.It Ql u
Undo last response.
.It Ql q
Exit without removing packages.
All changes will be lost.
.It Ql x
Save changes to
.Nm
database, remove unwanted packages, and exit without asking further questions.
.El
.Ss Command line options
.Bl -tag -width indent
.It Fl v, -verbose
.Nm
will show which packages have disappeared, have become dependencies
or (if
.Pa Quiet
is enabled) have become orphans.
.It Fl V, -version
Display version and copyright information.
.It Fl h, -help
Display a concise summary of the available options and argument syntax.
.It Fl f, -force
Don't ask anything and assume
.Ql no
as the answer to all questions.
It also installs any packages that seem to be missing,
thus forcing your system to comply with the
.Nm
database.
Can have
.Ql interesting
results if you're not careful.
.It Fl q, -quiet
Don't ask anything and assume
.Ql yes
as the answer to all questions.
Useful to create an initial
.Pa /var/lib/debfoster/keepers
file or to recreate it after changing the configuration file.
.It Fl m, -mark-only
Instructs debfoster to make changes to the keeper file but
not to actually install or delete any packages.
This can be used to
.Ql edit
a keeper file by invoking debfoster one or more times in a row.
The changes can then be committed by invoking debfoster with the
.Fl -force
option, which will delete/install any necessary packages.
This is mainly useful for scripts and frontends, but may be useful from
the command line as well.
.It Fl u, -upgrade
If used as
.Ql Nm Fl u Cm package
it will install or upgrade the packages specified on the command line
and try to upgrade all packages that it relies on.
.It Fl c, -config Ar file
Specify a different configuration file to use.
.It Fl k, -keeperfile Ar file
Specify a different
.Nm
database to use.
.It Fl n, -no-keeperfile
Don't read the
.Nm
database and start with an empty list.
.It Fl i, -ignore-default-rules
This will instruct
.Nm
to ignore the
.Va UseHold, UseEssential, MaxPriority, KeepSections, No and
.Va NokeepSections
settings in the config file (i.e., assume that any package can be an
orphan). This is a good option for those who really want to make sure
their system is squeaky clean. It's also useful when
sharing or transferring a keeper file between multiple machines where
different config files can cause some confusion. Properly used,
.Fl i
eliminates that uncertainty.
.It Fl a, -show-keepers
Lists the contents of the
.Nm
database.
.It Fl s, -show-orphans
List all orphaned packages that are not mentioned in the
.Nm
database.
.It Fl d, -show-depends Ar package
List all packages that this package depends on.
.It Fl e, -show-dependents Ar package
List all packages in the
.Nm
database that depend on this package.
.It Fl p, -show-providers Ar package
List all packages that provide
the dependency target specified by
.Ar package
(e.g.
.Qo Nm
-p x-terminal-emulator
.Qc ).
.It Fl r, -show-related Ar package
List all packages that are only installed because
this package depends on them.
.It Fl t, -use-tasks
Make tasks visible as packages. This will make tasks that are
selectable using tasksel(1) appear as packages named task-<label>.
.It Fl o, -option Ar opt=val
Override any configuration option specified in the
configuration file.
.El
.Sh CONFIGURATION
Some aspects of the behaviour of
.Nm
can be configured in the configuration file,
.Pa /etc/debfoster.conf .
Options are specified as
.Dl Va Option No = Li Value
Option names are case insensitive.
.Bl -tag -width indent
.It Va InstallCmd
Default:
.Pa apt-get install\p
Command invoked with a number of packages on the command line.
The command is not passed to
.Pa /bin/sh
but invoked like
.Xr xargs 1
with a number of packages as extra options.
.It Va RemoveCmd
Default:
.Pa apt-get --purge remove\p
Like
.Va InstallCmd
but for removing packages.
.It Va InfoCmd
Default:
.Pa dpkg -s\p
Like
.Va InstallCmd
but called with a single package as an argument to display information on.
.It Va KeeperFile
Default:
.Pa /var/lib/debfoster/keepers\p
The file where the list of orphans is stored.
You can use this file for reference when installing a machine
or even to make identical Debian installs.
.It Va DpkgStatus
Default:
.Pa /var/lib/dpkg/status\p
The file where
.Xr dpkg 8
stores its information about which packages are more or less installed.
This value can usually be left untouched.
.It Va DpkgAvailable
Default:
.Pa /var/lib/dpkg/available\p
The file where
.Xr dpkg 8
stores its information about which packages are available.
This value can usually be left untouched.
.It Va MaxPriority
Default:
.Pa standard\p
Any packages with a priority greater than this value will be
considered too basic to ask questions about.
The default value means that questions will be asked about packages
with priority "standard", "optional" and "extra".
With the special value
.Ql ANY
you can indicate that all
.Em known
priorities should be considered too important to ask questions about.
These priority values are known to
.Nm
(taken from the
.Pa debian-policy
package):
.Dl required
.Dl important
.Dl standard
.Dl optional
.Dl extra
.It Va UseHold
Default:
.Pa yes\p
Use the
.Pa hold
attribute from the
.Pa Status:
line.
Packages with this attribute won't ever be upgraded by apt, so it's safe
to assume that you want to keep it.
.It Va UseEssential
Default:
.Pa yes\p
Use the
.Pa Essential:
line from
.Xr dpkg 8 's
status file.
Most packages which are marked essential shouldn't be removed anyway,
so if you don't want to be bothered with it, enable this option.
.It Va UsePreDepends
Default:
.Pa yes\p
A package that pre-depends on another package requires the latter
during installation.
This option will make
.Nm
count these pre-dependencies as ordinary dependencies.
If you frequently update your packages you may want to keep an eye
out for pre-depended packages that have become obsolete.
.It Va UseRecommends
Default:
.Pa yes\p
Recommended packages would be installed together with the
package that recommends them in all usual setups.
This option will make
.Nm
count these recommendations as real dependencies.
Enabling this option will enable you to better manage packages which
were installed because another package recommended them.
.It Va UseSuggests
Default:
.Pa no\p
Packages suggested by another package usually enhance the function
of the latter or have a related function which may be useful in
combination with the package that suggested them.
This option will make
.Nm
count these suggestions as real dependencies.
Using this option will result in even fewer questions being asked.
.It Va UseTasks
Default:
.Pa no\p
Make tasks visible as packages. This will make tasks that are
selectable using tasksel(1) appear as packages named task-<label>.
.Nm
will treat them as if they were normal packages. Tasks cannot be
removed but marking a task for removal will stop
.Nm
asking questions about it.
.It Va KeepSections
Default:
.Pa \p
You may find that you are always interested in keeping
(for example) documentation.
With this option you can indicate that packages from a certain
section should always be kept.
You can specify a comma separated lists of
.Ql precious
sections.
.It Va NokeepSections
Default:
.Pa \p
List the sections you are never interested in.
For example,
.Ql libs
is a good candidate, as most libraries debfoster asks about
are leftovers from old packages.
.It Va GuessDepends
Default:
.Pa \p
List name extensions for packages that you want to group with their
base packages. Applications are often separated into multiple
packages with names like
.Qo app Qc , Qo app-doc Qc , Qo app-dev Qc . If you don't want to
answer questions about
.Qo app-doc Qc , you can add the Qo doc Qc extension to the
.Va GuessDepends
list.
.It Va NegativeKeepers
Default:
.Pa yes\p
Remember explicit removals of packages.
If a package is installed that has been explicitly removed before,
remove it again without asking.
Set this to
.Pa no
if you want to be asked anyway.
.It Va Verbose
Default:
.Pa no\p
Using this option has the same result as having
.Fl v
on the command line.
It will make
.Nm
show which packages have disappeared or have become a dependency.
.It Va Force
Default:
.Pa no\p
This option has the same meaning as the
.Fl f
command line option.
All orphaned packages are scheduled for removal without asking any question.
.It Va Quiet
Default:
.Pa no\p
Having this option (which has the same meaning as the
.Fl q
command line argument) in your configuration file
more or less defeats the purpose of
.Nm
although the 
.Pa KeeperFile
is still kept up-to-date.
.El
.Sh BUGS
Send reports to the Debian bug tracking system:\p
.Li http://bugs.debian.org/debfoster\p
with as much information as you can gather
(error messages, configuration files, versions of dpkg/apt, whatever
might be relevant).
A tool such as reportbug might come in handy.
.Sh SEE ALSO
.Xr apt-get 8 ,
.Xr apt-cache 8 ,
.Xr dpkg 8 ,
.Xr deborphan 1
.Xr tasksel 1
