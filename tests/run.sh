#! /bin/bash

debfoster=${DEBFOSTER:-../../src/debfoster}

set -e

for dir in ??? ; do
    test -d "$dir" || continue
    rm -f "$dir/out" "$dir"/*.out
    cp "$dir/keepers" "$dir/keepers.out"
    (cd "$dir" && $debfoster --verbose \
	-o DpkgStatus=status -o DpkgAvailable=available \
	-o KeeperFile=keepers.out \
	-o RemoveCmd='echo >>out "REMOVE: "' </dev/null 2>&1) >> "$dir/out"
    okay=true
    diff -u "$dir/keepers" "$dir/keepers.out" || okay=false
    diff -u "$dir/exp" "$dir/out" || okay=false
    $okay
done

rm -f ???/out ???/*.out
